'use client'

import { useState } from 'react'

const TreeNode = ({ node }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const handleToggle = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <>
      <div className="ml-4 transition-all">
        <div className="flex items-center mb-2">
          <span
            onClick={handleToggle}
            className={`cursor-pointer text-xl bg-[#7e3af2] text-white rounded-full h-6 w-6 flex items-center justify-center  mr-2 transform transition-transfor${isExpanded ? 'rotate-60' : 'rotate-0'}`}
          >
            {isExpanded ? '-' : '+'}
          </span>
          <div className="px-2 py-1 bg-[#7e3af2] rounded-lg">
            <span className="text-xl ">{node.rootData}</span>
          </div>

        </div>
        <div className={`ml-7 ${isExpanded ? 'block' : 'hidden'}`}>
          {node.children && node.children.length > 0 && (
            <div>
              {node.children.map((child) => (
                <TreeNode key={child.id} node={child} />
              ))}
            </div>
          )}
        </div>
      </div>
    </>
  );
};
export default TreeNode