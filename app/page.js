'use client'

import { useState } from 'react'

import dumyData from './_data/dumyData.json'
import TreeNode from "./components/TreeNode";

export default function Home() {
  const [show, setShow] = useState(false);
  const [menu, setMenu] = useState(false);

  return (
    <div className="min-h-screen flex items-center justify-center">
      <div className="max-w-md w-full">
        {dumyData.data.map((rootNode) => (
          <TreeNode key={rootNode.id} node={rootNode} />
        ))}
      </div>
    </div>
  )
}
